package steps;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.opencsv.CSVWriter;

public class Checklist 
{
	CSVWriter writer;
	List<String>data;
	WebElement row;
	String status;
	int row_count;
	WebDriver driver;
	//String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MTcyLCJmaWx0ZXIiOlsiYW5kIixbInRpbWUtaW50ZXJ2YWwiLFsiZmllbGQtaWQiLDYxMl0sImN1cnJlbnQiLCJkYXkiXSxbImNvbnRhaW5zIixbImZpZWxkLWlkIiw2MDFdLCIgaXMgb3V0c2lkZSBvcGVyYXRpb25hbCBhcmVhcyIseyJjYXNlLXNlbnNpdGl2ZSI6ZmFsc2V9XV0sImJyZWFrb3V0IjpbWyJmaWVsZC1pZCIsNjAxXSxbImZpZWxkLWlkIiw2MDZdXX0sImRhdGFiYXNlIjoyfSwiZGlzcGxheSI6InRhYmxlIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6eyJ0YWJsZS5jb2x1bW5zIjpbeyJuYW1lIjoiY29tbWVudCIsImZpZWxkUmVmIjpbImZpZWxkLWlkIiw2MDFdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJpZCIsImZpZWxkUmVmIjpbImZpZWxkLWlkIiw2MDZdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJhY3Rpb24iLCJmaWVsZFJlZiI6WyJmaWVsZC1pZCIsNjAwXSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoiYXNzaWduZWRfYnlfaWQiLCJmaWVsZFJlZiI6WyJmaWVsZC1pZCIsNjA0XSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoiYXNzaWduZWRfb24iLCJmaWVsZFJlZiI6WyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsNjE0XSwiZGVmYXVsdCJdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJhc3NpZ25lZF90b19pZCIsImZpZWxkUmVmIjpbImZpZWxkLWlkIiw2MDJdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJjaGVja2xpc3QiLCJmaWVsZFJlZiI6WyJmaWVsZC1pZCIsNjA1XSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoiY29tcGxldGVkX3N0ZXBzIiwiZmllbGRSZWYiOlsiZmllbGQtaWQiLDU5OV0sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6ImNyZWF0ZWRfYnlfaWQiLCJmaWVsZFJlZiI6WyJmaWVsZC1pZCIsNjAzXSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoiY3JlYXRlZF9kYXRlIiwiZmllbGRSZWYiOlsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDYxMl0sImRlZmF1bHQiXSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoiaXNfYWN0aXZlIiwiZmllbGRSZWYiOlsiZmllbGQtaWQiLDYxMF0sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6Im93bmVyX2lkIiwiZmllbGRSZWYiOlsiZmllbGQtaWQiLDU5OF0sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6InN0YXR1cyIsImZpZWxkUmVmIjpbImZpZWxkLWlkIiw2MDhdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJzdGF0dXNfZGF0ZSIsImZpZWxkUmVmIjpbImRhdGV0aW1lLWZpZWxkIixbImZpZWxkLWlkIiw2MDldLCJkZWZhdWx0Il0sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6InN0ZXAiLCJmaWVsZFJlZiI6WyJmaWVsZC1pZCIsNjExXSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoidG90YWxfc3RlcF9jb3VudCIsImZpZWxkUmVmIjpbImZpZWxkLWlkIiw2MDddLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJ1cGRhdGVkX2RhdGUiLCJmaWVsZFJlZiI6WyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsNjEzXSwiZGVmYXVsdCJdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJpZCIsImZpZWxkX3JlZiI6WyJmaWVsZC1pZCIsNjA2XSwiZW5hYmxlZCI6dHJ1ZX1dfX0=";
	String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjE3MiwiZmlsdGVyIjpbImFuZCIsWyJ0aW1lLWludGVydmFsIixbImZpZWxkLWlkIiw2MTJdLC0xLCJkYXkiLHt9XSxbImNvbnRhaW5zIixbImZpZWxkLWlkIiw2MDFdLCIgaXMgb3V0c2lkZSBvcGVyYXRpb25hbCBhcmVhcyIseyJjYXNlLXNlbnNpdGl2ZSI6ZmFsc2V9XV0sImJyZWFrb3V0IjpbWyJmaWVsZC1pZCIsNjAxXSxbImZpZWxkLWlkIiw2MDZdXX0sInR5cGUiOiJxdWVyeSJ9LCJkaXNwbGF5IjoidGFibGUiLCJ2aXN1YWxpemF0aW9uX3NldHRpbmdzIjp7InRhYmxlLmNvbHVtbnMiOlt7Im5hbWUiOiJjb21tZW50IiwiZmllbGRSZWYiOlsiZmllbGQtaWQiLDYwMV0sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6ImlkIiwiZmllbGRSZWYiOlsiZmllbGQtaWQiLDYwNl0sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6ImFjdGlvbiIsImZpZWxkUmVmIjpbImZpZWxkLWlkIiw2MDBdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJhc3NpZ25lZF9ieV9pZCIsImZpZWxkUmVmIjpbImZpZWxkLWlkIiw2MDRdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJhc3NpZ25lZF9vbiIsImZpZWxkUmVmIjpbImRhdGV0aW1lLWZpZWxkIixbImZpZWxkLWlkIiw2MTRdLCJkZWZhdWx0Il0sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6ImFzc2lnbmVkX3RvX2lkIiwiZmllbGRSZWYiOlsiZmllbGQtaWQiLDYwMl0sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6ImNoZWNrbGlzdCIsImZpZWxkUmVmIjpbImZpZWxkLWlkIiw2MDVdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJjb21wbGV0ZWRfc3RlcHMiLCJmaWVsZFJlZiI6WyJmaWVsZC1pZCIsNTk5XSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoiY3JlYXRlZF9ieV9pZCIsImZpZWxkUmVmIjpbImZpZWxkLWlkIiw2MDNdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJjcmVhdGVkX2RhdGUiLCJmaWVsZFJlZiI6WyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsNjEyXSwiZGVmYXVsdCJdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJpc19hY3RpdmUiLCJmaWVsZFJlZiI6WyJmaWVsZC1pZCIsNjEwXSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoib3duZXJfaWQiLCJmaWVsZFJlZiI6WyJmaWVsZC1pZCIsNTk4XSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoic3RhdHVzIiwiZmllbGRSZWYiOlsiZmllbGQtaWQiLDYwOF0sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6InN0YXR1c19kYXRlIiwiZmllbGRSZWYiOlsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDYwOV0sImRlZmF1bHQiXSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoic3RlcCIsImZpZWxkUmVmIjpbImZpZWxkLWlkIiw2MTFdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJ0b3RhbF9zdGVwX2NvdW50IiwiZmllbGRSZWYiOlsiZmllbGQtaWQiLDYwN10sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6InVwZGF0ZWRfZGF0ZSIsImZpZWxkUmVmIjpbImRhdGV0aW1lLWZpZWxkIixbImZpZWxkLWlkIiw2MTNdLCJkZWZhdWx0Il0sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6ImlkIiwiZmllbGRfcmVmIjpbImZpZWxkLWlkIiw2MDZdLCJlbmFibGVkIjp0cnVlfV19fQ==";
	String element,arr[]= {" "," "},columName[]= {"COMMENT","ID"};
	int i,j;
	boolean test=false;
	public Checklist(WebDriver driver,CSVWriter writer,List<String[]> data2) throws IOException, InterruptedException
	{
		driver.get(Url);
		Thread.sleep(5000);

		String element=null;
		try {
			element=(driver.findElement(By.xpath("//*[ contains (text(), 'No results!' ) ]")).getText().toString());
		} catch(Exception e) {}

		if(element != null)
		{ 
			test = false;
			System.out.println(element);
			Assert.assertTrue(test, "Data are not found");

		} 

		else {
			test=true;
			writer.writeNext(columName);
			List<WebElement>  rows = driver.findElements(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div"));
			row_count=rows.size();
			System.out.println("No of rows : " +row_count);
			for(i=1,j=2;i<=row_count-1 && j<=row_count; i+=2,j+=2)

			{
				arr[0]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div["+(i)+"]")).getText().toString();
				arr[1]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div["+j+"]")).getText().toString();
				writer.writeNext(arr);
			}
			writer.flush();
		}
	}
}


























